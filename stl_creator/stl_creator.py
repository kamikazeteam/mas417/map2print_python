import threading

from stl import mesh
import numpy as np
from scipy.spatial import Delaunay
from scipy.ndimage import gaussian_filter
from PIL import ImageOps


class StlCreator:

    thickness = 50 # TODO(martin): set this from somwhere?

    def __init__(self, terrain_data, quality):       
        self.terrain_data_ = terrain_data[::-1]
        self.quality_ = int(100/quality)

        self.threads_ = [threading.Thread(target=self.__create_mesh_top),
                         threading.Thread(target=self.__create_mesh_bottom),
                         threading.Thread(target=self.__create_mesh_sides)]


    def __spin_threads(self):
        for thread in self.threads_:
            thread.start()
        
    
    def __stop_threads(self):
        for thread  in self.threads_:
            thread.join()


    def __smooth_data(self, sigma, n):
        for _ in range(n):
            self.terrain_data_ = gaussian_filter(self.terrain_data_, sigma=sigma)

    
    def __create_vertices_arrays(self):
        ''' Create array of points from matrix from left to right, row by row. '''
        self.width = len(self.terrain_data_[0][::self.quality_])
        self.height = len(self.terrain_data_[::self.quality_])  

        self.top_vertices = [[0]] * self.width * self.height
        self.bottom_vertices = [[0]] * self.width * self.height
        self.side_vertices_yz0 = [[0]] * self.height
        self.side_vertices_yz1 = [[0]] * self.height

        x = 0
        y = 0
        index = 0
        for a, row in enumerate(self.terrain_data_[::self.quality_]):
            x = 0
            for height in row[::self.quality_]:
                h = height
                self.top_vertices[index] = [x, y, height]
                self.bottom_vertices[index] = [x, y, -self.thickness]
                index += 1
                x += self.quality_
            self.side_vertices_yz0[a] = [0, y, row[0]]
            self.side_vertices_yz1[a] = [(self.width-1)*self.quality_, y, h]
            y += self.quality_
                
        self.side_vertices_xz0 = self.top_vertices[:self.width]
        self.side_vertices_xz1 = self.top_vertices[-self.width:]


    def __vertices_ok(self):
        ''' Returns false if less than 1 possible face. '''
        self.num_faces = 2 * (self.width*self.height-self.width-self.height+1)
        if self.num_faces < 1:
            print('Error: not enough vertices to create mesh.')
            return False
        return True
        

    def __create_mesh_top(self):
        # Triangulate the top surface
        top_vertices_2d = [point[:2] for point in self.top_vertices]
        self.top_faces = Delaunay(top_vertices_2d).simplices
        
        # Create meshes from the triangulations
        self.top_mesh = mesh.Mesh(np.zeros(self.top_faces.shape[0], dtype=mesh.Mesh.dtype))
        for i, f in enumerate(self.top_faces):
            for j in range(3):
                self.top_mesh.vectors[i][j] = self.top_vertices[f[j]]
    

    def __create_mesh_bottom(self):
        ''' Triangulate the bottom surface, uses same x and y values as the top surface,
            but with z-value equal to the defined thickness. '''
        bottom_vertices_2d = [point[:2] for point in self.bottom_vertices]
        self.bottom_faces = Delaunay(bottom_vertices_2d).simplices
        #self.bottom_vertices = [[*xy, -self.thickness] for xy in self.top_vertices_2d]

        self.bottom_mesh = mesh.Mesh(np.zeros(self.bottom_faces.shape[0], dtype=mesh.Mesh.dtype))
        for i, f in enumerate(self.bottom_faces):
            for j in range(3):
                self.bottom_mesh.vectors[i][j] = self.bottom_vertices[f[j]]


    def __create_mesh_sides(self):
        ''' Triangulate the side surfaces. '''
        # xz0 vertices (xz-plane at y=0)
        side_vertices_xz0_lower = [[pt[0], 0, -self.thickness] for pt in self.side_vertices_xz0]
        self.side_vertices_xz0 = self.side_vertices_xz0 + side_vertices_xz0_lower

        # xz1 vertices (xz-plane at y=ymax)
        side_vertices_xz1_lower = [[pt[0], (self.height-1)*self.quality_, -self.thickness] for pt in self.side_vertices_xz1]
        self.side_vertices_xz1 = self.side_vertices_xz1 + side_vertices_xz1_lower

        # yz0 vertices (yz-plane at x=0)
        side_vertices_yz0_lower = [[0, pt[1], -self.thickness] for pt in self.side_vertices_yz0]
        self.side_vertices_yz0 = self.side_vertices_yz0 + side_vertices_yz0_lower

        # yz1 vertices (yz-plane at x=xmax)
        side_vertices_yz1_lower = [[(self.width-1)*self.quality_, pt[1], -self.thickness] for pt in self.side_vertices_yz1]
        self.side_vertices_yz1 = self.side_vertices_yz1 + side_vertices_yz1_lower

        # xz faces, applies for both xz0 and xz1
        width_xz = int(len(self.side_vertices_xz0)/2)
        self.side_faces_xz = np.zeros([len(self.side_vertices_xz0)-2, 3], dtype=int)
        index = 0
        for i in range(width_xz-1):
            self.side_faces_xz[index] = [i, i+1, width_xz+i]
            index += 1
            self.side_faces_xz[index] = [i+1, width_xz+i+1, width_xz+i]
            index += 1
        
        # yz faces, applies for both yz0 and yz1
        width_yz = int(len(self.side_vertices_yz0)/2)
        self.side_faces_yz = np.zeros([len(self.side_vertices_yz0)-2, 3], dtype=int)
        index = 0
        for i in range(width_yz-1):
            self.side_faces_yz[index] = [i, i+1, width_yz+i]
            index += 1
            self.side_faces_yz[index] = [i+1, width_yz+i+1, width_yz+i]
            index += 1

        self.side_mesh_xz0 = mesh.Mesh(np.zeros(self.side_faces_xz.shape[0], dtype=mesh.Mesh.dtype))
        self.side_mesh_xz1 = mesh.Mesh(np.zeros(self.side_faces_xz.shape[0], dtype=mesh.Mesh.dtype))
        for i, f in enumerate(self.side_faces_xz):
            for j in range(3):
                self.side_mesh_xz0.vectors[i][j] = self.side_vertices_xz0[f[j]]
                self.side_mesh_xz1.vectors[i][j] = self.side_vertices_xz1[f[j]]
        
        self.side_mesh_yz0 = mesh.Mesh(np.zeros(self.side_faces_yz.shape[0], dtype=mesh.Mesh.dtype))
        self.side_mesh_yz1 = mesh.Mesh(np.zeros(self.side_faces_yz.shape[0], dtype=mesh.Mesh.dtype))
        for i, f in enumerate(self.side_faces_yz):
            for j in range(3):
                self.side_mesh_yz0.vectors[i][j] = self.side_vertices_yz0[f[j]]
                self.side_mesh_yz1.vectors[i][j] = self.side_vertices_yz1[f[j]]


    def __combine_meshes(self):
        self.terrain_mesh = mesh.Mesh(np.concatenate([self.top_mesh.data,
                                                      self.bottom_mesh.data,
                                                      self.side_mesh_xz0.data,
                                                      self.side_mesh_xz1.data,
                                                      self.side_mesh_yz0.data,
                                                      self.side_mesh_yz1.data]))

        self.terrain_mesh.save('mesh.stl') # TODO(martin): make this dynamic, and maybe not overwrite last?


    def run(self):
        self.__smooth_data(1, 5)  
        self.__create_vertices_arrays()
        if self.__vertices_ok():
            self.__spin_threads()
            self.__stop_threads()
            self.__combine_meshes()