from stl_creator.stl_creator import StlCreator
from fetch_terrain_data.fetch_terrain_data import FetchTerrainData


bbox_joronatten1 = [59.4659937, 8.864695, 59.4968018, 8.904229]
bbox_joronatten2 = [59.471700, 8.867057, 59.492525, 8.904004]
bbox_norge = [57.925582, 4.279257, 71.435480, 32.141712]
bbox_seljord = [59.5211037, 8.6728462, 59.595528, 8.759435]
bbox_vestlandet = [60.0181543, 6.6794267, 61.3548221, 7.1952795]
bbox_rjukan = [59.8652929, 8.5654643, 59.9826571, 8.9109777]
bbox_kvadraturen = [58.1419818818308, 7.985405127492239, 58.15194818818304, 8.005405127492239]

bbox_vestlandet1 = [61.678635647072525, 5.582493706376555, 61.98163564707252, 7.157493706376554]

bbox_bygland = [58.74022572214747, 7.750690412033273, 58.80922572214749, 7.882690412033273]

fetch_obj = FetchTerrainData(debug=True)
data = fetch_obj.fetch_terrain_data(bbox_bygland, 1)


stl_obj = StlCreator(data, 50)
stl_obj.run()
