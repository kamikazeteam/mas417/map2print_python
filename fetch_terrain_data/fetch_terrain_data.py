from io import BytesIO

import numpy as np
from numpy.core.records import array
import requests
import math
from PIL import Image

#Creating FetchTerrainData class.
class FetchTerrainData():
    #Init function, 
    def __init__(self, debug):
        #Creating instance variable debug
        self.debug = debug
        self.a = 6378137.0  # equatorial radius
        self.e2 = 0.00669437999014  # eccentricity squared

    def __find_height_width(self, bbox):
        height_deg = (bbox[2] - bbox[0])
        width_deg = (bbox[3] - bbox[1])

        if self.debug:
            print(f"Init width deg: {width_deg}. Init height deg: {height_deg}")

        lat = math.radians(bbox[0] + height_deg/2) # center bbox latitude degree

        height_m = height_deg * (math.pi*self.a*(1-self.e2))/(180*(1-self.e2*(math.sin(lat))**2)**(3/2))    # convert to height and width degrees to meters
        width_m = width_deg * (math.pi*self.a*math.cos(lat))/(180*math.sqrt(1-self.e2*(math.sin(lat))**2))  # based on formulas from https://en.wikipedia.org/wiki/Latitude

        if self.debug:
            print(f"Init width meter: {width_m}. Init height meter: {height_m}")

        return height_m, width_m


    def __scale_height_width(self, height, width):
        scale = 2000/max(width, height) #Scaling to get a maximum image width/height to 2000 pixels

        height = int(height * scale)
        width = int(width * scale)

        if self.debug:
            print(f"Width after scaling: {width}. Height after scaling: {height}. Scale: {scale}")

        return height, width, scale


    def __create_bbox_string(self, bbox):
        bbox_string = ''
        
        for i, pos in enumerate(bbox):
            if i < len(bbox) - 1:
                bbox_string = bbox_string + str(pos) + ','
            else:
                bbox_string = bbox_string + str(pos)

        return bbox_string

    def __fetch_api_data(self, height, width, bbox_string):
        url = 'https://wms.geonorge.no/skwms1/wms.hoyde-dom?'
        query = {'service': 'WMS',
                 'version': '1.3.0',
                 'request': 'GetMap',
                 'format': 'image/png',
                 'transparent': 'false',
                 'layers': 'DOM:None',
                 'crs': 'EPSG:4258',
                 'styles': '',
                 'width': str(width),
                 'height': str(height),
                 'bbox': bbox_string}

        response = requests.get(url, params=query, verify=True)

        if self.debug:
            print(f"HTTP response status code = {response.status_code}")
        
        return response

    def __create_image_array(self, response):
        img = Image.open(BytesIO(response.content))
        np_img = np.asarray(img)

        return np_img

    def __create_elevation_data(self, height, width, scale, img_arr, height_scale):
        array = np.zeros(width*height)
        elevation_data = np.array(array).reshape(height, width)
        scale_factor = 2539 / 255 #The tallest mountain of Norway (Galdhøpiggen (2469 m MSL (+70m))) is pure white, sclaing other pixels from this value.
        #Scale got from url: https://wms.geonorge.no/skwms1/wms.hoyde-dom?request=GetLegendGraphic%26version%3D1.3.0%26format%3Dimage%2Fpng%26layer%3DDOM%3ANone

        for y, row in enumerate(img_arr):
            for x, element in enumerate(row):
                elevation_data[y, x] = int(element[0]*scale_factor*scale*height_scale)

        max_height = elevation_data.max()
        min_height = elevation_data.min()

        elevation_data = np.subtract(elevation_data, min_height)

        if self.debug:
            img = Image.fromarray(np.uint8(img_arr))
            img.show()
            print(f"Max height found: {max_height}. Min height found: {min_height}. Scale factor: {scale_factor}")

        return elevation_data

    def fetch_terrain_data(self, bbox, height_scale):
        
        bbox_string = self.__create_bbox_string(bbox)

        height_m, width_m = self.__find_height_width(bbox)
        height, width, scale = self.__scale_height_width(height_m, width_m)

        response = self.__fetch_api_data(height, width, bbox_string)

        img_arr = self.__create_image_array(response)

        elevation_data = self.__create_elevation_data(height, width, scale, img_arr, height_scale)

        return elevation_data

if __name__ == "__main__":
    #bbox = [57.925582, 4.279257, 71.435480, 32.141712]  # Jøronatten
    bbox = [59.471700, 8.867057, 59.492525, 8.904004]
    FTD = FetchTerrainData(debug=True)
    data = FTD.fetch_terrain_data(bbox)
    print(data)
